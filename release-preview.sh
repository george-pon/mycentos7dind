#!/bin/bash
#
# before release
#

bash build-image.sh

# docker for windows 2.0.0
DOCKER_HOST=tcp://127.0.0.1:2375

# set WINPTY_CMD environment variable when it need.
function check_winpty() {
    if type tty.exe > /dev/null ; then
        if type winpty.exe > /dev/null ; then
            local ttycheck=$( tty | grep "/dev/pty" )
            if [ ! -z "$ttycheck" ]; then
                export WINPTY_CMD=winpty
                return 0
            else
                export WINPTY_CMD=
                return 0
            fi
        fi
    fi
    return 0
}
check_winpty 2>/dev/null

$WINPTY_CMD docker run -i -t --rm  \
    --name  mydind  \
    -e http_proxy=$http_proxy \
    -e https_proxy=$https_proxy \
    -e no_proxy=$no_proxy \
    -e DOCKER_HOST=$DOCKER_HOST \
    mycentos7dind:latest \
    bash
