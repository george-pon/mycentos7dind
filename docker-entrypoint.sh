#!/bin/bash

set -e

echo "arg is $@"

while true
do
    if [ x"$1"x = x"-e"x ]; then
        eval $2
        export ${2%%=*}
        mkdir -p /etc/profile.d
        echo "export $2" >> /etc/profile.d/docker-entrypoint-env.sh
        shift
        shift
        continue
    fi
    break
done

# if we have "--link some-docker:docker" and not DOCKER_HOST, let's set DOCKER_HOST automatically
if [ -z "$DOCKER_HOST" -a "$DOCKER_PORT_2375_TCP" ]; then
    export DOCKER_HOST='tcp://docker:2375'
fi

if [ $# -eq 0 ]; then
    /bin/bash
else
    "$@"
fi
